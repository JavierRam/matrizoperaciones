﻿using System;
using System.ComponentModel.Design;

int row_max = 5;
int col_max = 5;
int[][] matriz = new int[row_max][];

int padding = 5;

matriz[0] = new int[] { 12, 6, 9, 4, 26 };
matriz[1] = new int[] { 22, 16, 11, 4, 2 };
matriz[2] = new int[] { 56, 890, 23, 4, 6 };
matriz[3] = new int[] { 2, 12, 91, 4, 11 };
matriz[4] = new int[] { 1, 16, 19, -4, 99 };

Console.WriteLine("0.- TABLA DE CONTENIDO");

for (int row = 0; row < row_max; row++)
{
    for (int col = 0; col < row_max; col++)
    {
        Console.Write("" + matriz[row][col] + ",");
    }
    Console.WriteLine();
}

Console.WriteLine();
Console.WriteLine("1.- TABLA DE CONTENIDO FORMATO");
for(int row = 0; row <row_max; row++)
{
    for (int col = 0; col < col_max; col++)
    {
        Console.Write(matriz[row][col].ToString().PadRight(padding));
    }
    Console.WriteLine();
}

Console.WriteLine();
Console.WriteLine("2.- TABLA DE POSICIONES");

for(int i = 0; i < row_max; i++)
{
    Console.Write(" COL " + i + " ".ToString().PadRight(padding)); 
}
Console.WriteLine(); 

for (int j = 0; j < row_max; j++)
{
    for (int i = 0; i < row_max; i++)
    {
        Console.Write("[" + j + "," + i + "] " ); 
    }
    Console.WriteLine(); 
}


Console.WriteLine();
Console.WriteLine("3.- DIAGONAL INVERTIDA ");

for (int i = 0; i < row_max; i++)
{
    for (int j = 0; j < col_max; j++)
    {
        if (i == j)
        {
            Console.Write("[" + i + "," + j + "] ", matriz[i] [j].ToString().PadRight(padding));
        }
        else
        {
            Console.Write("  "); 
        }
    }
    Console.WriteLine(); 
}


Console.WriteLine();
Console.WriteLine("4.- DIAGONAL / ");

for (int j = 0; j < row_max; j++)
{
    for (int i = 0; i < row_max; i++)
    {
        if (j + i == row_max - 1)
        {
            Console.Write("[" + j + "," + i + "] " ,matriz[j][i] + " " + "\t");
        }
        else
        {
            Console.Write("  ");
        }
    }
    Console.WriteLine();
}


Console.WriteLine();

Console.WriteLine("5.- COLUMNA");

int columna_seleccionada = 0; 

Console.WriteLine("Columna seleccionada: " + columna_seleccionada);
for (int fila = 0; fila < row_max; fila++)
{
    Console.WriteLine("[" + matriz[fila][columna_seleccionada] + "]" + "\t");
}